package bond.whatwherewhenapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

/**
 * HomePage: Start of the user interface
 */
public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }

}
