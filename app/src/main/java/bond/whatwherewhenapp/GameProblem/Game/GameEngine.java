package bond.whatwherewhenapp.GameProblem.Game;

import bond.whatwherewhenapp.GameProblem.QuestionProblem.QuestionPackGenerator;
import bond.whatwherewhenapp.GameProblem.Result.ResultEngine;

/**
 * Основные процессы игры
 */

public class GameEngine {

    /**
     * Конструктор игры
     * @param questionPackSize
     */
    public GameEngine(int questionPackSize)
    {
    	startGame(questionPackSize);
    	if(questionPackSize == 0) {
    		showResult();
    	}
    }

    /**
     * Начало игры
     * @param questionPackSize
     */
    public void startGame(int questionPackSize)
    {
        QuestionPackGenerator questionPackGenerator = new QuestionPackGenerator(questionPackSize);
        questionPackGenerator.getPack();

    }

    /**
     * Конец игры
     */
    public void showResult()
    {
        ResultEngine resultEngine = new ResultEngine(12, 12, 12, 12);

    }
}
