package bond.whatwherewhenapp.GameProblem;

import android.support.annotation.NonNull;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import bond.whatwherewhenapp.GameProblem.QuestionProblem.Question;

/**
 * Created by bonda on 25-Dec-17.
 */

public class FakeQuestionModel {
    List<Question> questionList = new List<Question>() {
        @Override
        public int size() {
            return 0;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean contains(Object o) {
            return false;
        }

        @NonNull
        @Override
        public Iterator<Question> iterator() {
            return null;
        }

        @NonNull
        @Override
        public Object[] toArray() {
            return new Object[0];
        }

        @NonNull
        @Override
        public <T> T[] toArray(@NonNull T[] a) {
            return null;
        }

        @Override
        public boolean add(Question question) {
            return false;
        }

        @Override
        public boolean remove(Object o) {
            return false;
        }

        @Override
        public boolean containsAll(@NonNull Collection<?> c) {
            return false;
        }

        @Override
        public boolean addAll(@NonNull Collection<? extends Question> c) {
            return false;
        }

        @Override
        public boolean addAll(int index, @NonNull Collection<? extends Question> c) {
            return false;
        }

        @Override
        public boolean removeAll(@NonNull Collection<?> c) {
            return false;
        }

        @Override
        public boolean retainAll(@NonNull Collection<?> c) {
            return false;
        }

        @Override
        public void clear() {

        }

        @Override
        public Question get(int index) {
            return null;
        }

        @Override
        public Question set(int index, Question element) {
            return null;
        }

        @Override
        public void add(int index, Question element) {

        }

        @Override
        public Question remove(int index) {
            return null;
        }

        @Override
        public int indexOf(Object o) {
            return 0;
        }

        @Override
        public int lastIndexOf(Object o) {
            return 0;
        }

        @Override
        public ListIterator<Question> listIterator() {
            return null;
        }

        @NonNull
        @Override
        public ListIterator<Question> listIterator(int index) {
            return null;
        }

        @NonNull
        @Override
        public List<Question> subList(int fromIndex, int toIndex) {
            return null;
        }
    };

    public FakeQuestionModel() {
        //Generate own questions
    }
}
