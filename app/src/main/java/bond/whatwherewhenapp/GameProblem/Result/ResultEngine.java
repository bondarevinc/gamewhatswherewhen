package bond.whatwherewhenapp.GameProblem.Result;

/**
 * Created by bonda on 11-Dec-17.
 */

public class ResultEngine {
    /**
     * Пользовательские ответы, правильные ответы
     */
    private int yourAnswers, correctAnswers;
    /**
     * Пользовательское время, правильное время
     */
    private int yourTime, correctTime;

    /**
     * Генерация статистики в виде результата
     * @param yourAnswers
     * @param correctAnswers
     * @param yourTime
     * @param correctTime
     */
    public ResultEngine(int yourAnswers, int correctAnswers, int yourTime, int correctTime) {
        this.yourAnswers = yourAnswers;
        this.correctAnswers = correctAnswers;
        this.yourTime = yourTime;
        this.correctTime = correctTime;
    }

    /**
     * Статистика по ответам
     * @return
     */
    public int getAnswerStatistics()
    {
        return 0;
    }

    /**
     * Статистика по времени
     * @return
     */

    public int getTimeStatistics()
    {
        return 0;
    }


}
