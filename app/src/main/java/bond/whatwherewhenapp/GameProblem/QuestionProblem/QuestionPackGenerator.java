package bond.whatwherewhenapp.GameProblem.QuestionProblem;

import java.util.ArrayList;
import java.util.List;

import bond.whatwherewhenapp.ServerEngine;

/**
 * Created by bonda on 11-Dec-17.
 */

public class QuestionPackGenerator {

    /**
     * Пак вопросов
     */
    private List<Question> packOfQuestion = new ArrayList<Question>();

    /**
     * Создание объекта пака вопросов
     * @param questionPackSize
     */
    public QuestionPackGenerator(int questionPackSize)
    {
        generatePack(questionPackSize);
    }

    /**
     * Метод возвращает пак вопросов
     * @return
     */
    public List<Question> getPack()
    {
        return packOfQuestion;
    }

    /**
     * Генерация пака вопросов
     * @param packSize
     */
    private void generatePack(int packSize)
    {
        for (int i = 0; i < packSize; i++) {
            Question questionExample = getQuestion();
            packOfQuestion.add(i, questionExample);
        }
    }

    /**
     * Получение случайного вопроса с сервера
     * @return
     */
    private Question getQuestion()
    {
        ServerEngine serverEngine = new ServerEngine();
        return null;
    }
}
