package bond.whatwherewhenapp.GameProblem.QuestionProblem;

/**
 * Created by bonda on 25-Dec-17.
 */

public class Question {

    private String author = null;
    private String textQuestion = null;
    private String commentary = null;
    private String date = null;

    public String getAuthor() {
        return author;
    }

    public String getTextQuestion() {
        return textQuestion;
    }

    public String getCommentary() {
        return commentary;
    }

    public String getDate() {
        return date;
    }
}
